<?php
require_once 'class/Messages.php';
require_once 'class/GuestBook.php';

function isAjax() {
	return !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
}

$errors    = null;
$success   = false;
$guestbook = new GuestBook(__DIR__ . DIRECTORY_SEPARATOR . 'datas' . DIRECTORY_SEPARATOR . 'messages');
if (isset($_POST['username'], $_POST['message'])) {
	$message = new Message($_POST['username'], $_POST['message']);
	if ($message->isValid()){
		$guestbook->addMessage($message);
		$success = true;

	} else {
		$errors = $message->getErrors();
	}
}
if (!empty($errors) && isAjax()) {
	echo json_encode($errors);
	header('Content-Type: application/json');
	http_response_code(400);
	die();
}
$messages      = $guestbook->getMessages();
$countMessages = count($messages);
?>
<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="theme-color" content="#2D5EEB" />
	<meta name="msapplication-navbutton-color" content="#2D5EEB" />
	<title>Livre d'or | Attig.fr</title>
	<meta property="og:title" content="Workshop Livre d'or" />
	<meta property="og:site_name" content="Attig.fr" />
	<meta property="og:url" content="http://www.attig.fr/" />
	<meta property="og:language" content="fr" />
	<meta property="og:description" content="Worshop : realisation d'un module de livre d'or sous php" />
	<meta property="og:image" content="app/ogg-image.png" />
	<meta name="author" content="Mourad Attig" /> 
	<link rel="shortcut icon" type="image/png" href="app/favicon.png" name="Student Male icon by Icons8" />
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous" />
	<link rel="stylesheet" href="app/main.css" />
	<script src="https://kit.fontawesome.com/f4bcc61421.js" crossorigin="anonymous"></script>
	<body>

	<div class="modal fade" id="modalfeedback" tabindex="-1" role="dialog" aria-labelledby="ModalFeedback" aria-hidden="true">
  		<div class="modal-dialog" role="document">
    		<div class="modal-content">
      			<div class="modal-header">
        			<h5 class="modal-title" id="modalfeedback">Votre avis est important pour moi !</h5>
        			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          				<span aria-hidden="true">&times;</span>
        			</button>
      			</div>
      			<div class="modal-body justify">
				  Il est important pour moi de me remettre constamment en question. C'est pour cette raison que je souhaite avoir un retour sur tous mes projets afin de m'améliorer.<br> Si vous souhaitez participer merci de bien vouloir m'envoyer un petit mail à <a href="mailto:mouradattig06560@gmail.com">mouradattig06560@gmail.com</a>. <br> Je vous en serai vraiment très reconnaissant..
      			</div>
      			<div class="modal-footer">
        			<button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
      			</div>
    		</div>
  		</div>
	</div>

	<a href="#" id="feedback" data-toggle="modal" data-target="#modalfeedback"><div class="feedback">Votre avis</div></a>
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb">
				 <li class="breadcrumb-item active strong" aria-current="page">Livre d'or</li>
			</ol>
		</nav>
	<div class="container">

	<div class="card">
  		<div class="card-header">
		  <i class="fas fa-info-circle mr-2"></i> Scenario d'utilisation
  		</div>
  		<div class="card-body">
    	<p class="card-text">Module de commentaire sur un site de photographe independant.</p>
  		</div>
	</div>

	<?php if (!empty($errors)):?>
		<div class="alert alert-danger">
		<i class="fas fa-exclamation-triangle mr-2"></i> Formulaire invalide
		</div>
	<?php endif; ?>

	<?php if ($success):?>
		<div class="alert alert-success">
		<i class="far fa-thumbs-up mr-2"></i>Merci pour votre message, il a bien ete envoyer
		</div>
	<?php endif; ?>

		<form action="" method="POST" id="guestbook" class="form">
			<div class="form-group">
				<label for="InputUsername">Pseudo :</label>
				<input type="text" class="form-control  <?= isset($errors['username']) ? 'is-invalid' : '' ?>" autofocus="true" id="InputUsername" name="username" required>
				<?php if (isset($errors['username'])): ?>
            		<div class="invalid-feedback"><?= $errors['username'] ?></div>
            	<?php endif ?>
			</div>
			<div class="form-group">
				<label for="InputMessage">Message :</label>
				<textarea class="form-control <?= isset($errors['message']) ? 'is-invalid' : '' ?>" id="InputMessage" name="message" rows="3" required></textarea>
				<?php if (isset($errors['message'])): ?>
            		<div class="invalid-feedback"><?= $errors['message'] ?></div>
            	<?php endif ?>
			</div>
			<button type="submit" class="btn btn-primary">Envoyer</button>
		</form>	
	</div>

	<div class="container">
		<?php if (!empty($messages)): ?>
			<h2>Vos messages <span class="badge badge-secondary"><?= $countMessages ?></span></h2>
		<?php endif ?>
		<div class="card-columns">
			<?php foreach ($messages as $message): ?>
			<?= $message->renderHtml() ?>
			<?php endforeach ?>
		</div>
	</div>
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	</body>
</html>