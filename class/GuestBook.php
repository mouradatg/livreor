<?php 
require_once 'Messages.php';

class GuestBook {

    private $file;
    
    public function __construct(string $file)
    {
        $dir = dirname($file);
        if (!is_dir($dir)) {
            mkdir($dir, 0777, true);
        }
        if (!file_exists($file)) {
            touch($file);
        }
        $this->file = $file;
    }

    public function addMessage(Message $message)
    {
        file_put_contents($this->file, $message->toJson() . "\n", FILE_APPEND);
    }

    public function getMessages(): array
    {
        $content  = trim(file_get_contents($this->file));
        $lines    = explode("\n", $content);
        $messages = [];
        foreach ($lines as $line) {
            $messages[] = Message::fromJSON($line);
        }
        return array_reverse($messages);
    }
}