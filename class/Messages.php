<?php
class Message {

  private $username;
  private $message;
  private $date;

  public static function fromJSON(string $json): Message
  {
    $data = json_decode($json, true);
    return new self($data['username'], $data['message'], new DateTime("@" . $data['date']));
  }

  public function __construct(string $username, string $message, ?DateTime $date = null)
  {
    $this->username = $username;
    $this->message  = $message;
    $this->date     = $date ?: new DateTime();
  }

  public function isValid(): bool
  {
    return empty($this->getErrors());
  }

  public function getErrors(): array
  {
    $errors = [];
    if (strlen($this->username) < 3) {
      $errors['username'] = "Votre pseudo est trop court";
    }
    if (strlen($this->message) < 10) {
      $errors['message'] = "Votre message est trop court";
    }
    return $errors;
  }

  public function renderHtml(): string
  {
    $username = htmlentities($this->username);
    $content  = nl2br(htmlentities($this->message));
    $this->date->setTimezone(new DateTimeZone('Europe/Paris'));
    $date = $this->date->format('d-m-Y | H:i');
    return <<<HTML
<div class="card">
    <div class="card-body">
      <h5 class="card-title"><i class="far fa-user mr-1"></i>{$username}</h5>
      <p class="card-text text-justify">{$content}</p>
    </div>
    <div class="card-footer">
      <i class="fas fa-hourglass-start mr-2"></i><small class="text-muted">{$date}</small>
    </div>
</div>
HTML;
  }

  public function toJson(): string
  {
    return json_encode([
      'username' => $this->username,
      'message'  => $this->message,
      'date'     => $this->date->getTimestamp()
    ]);
  }

}